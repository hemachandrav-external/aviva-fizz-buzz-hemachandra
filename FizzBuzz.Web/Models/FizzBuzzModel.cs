﻿// <copyright file="FizzBuzzModel.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FizzBuzz.Web.Models
{
    /// <summary>
    /// Model class for fizz buzz
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Property for user input number
        /// </summary>
        [Required(ErrorMessage = "Please enter a number")]
        [Range(1, 1000, ErrorMessage = "Please enter a number between 1 and 1000")]
        public int UserInput { get; set; }

        /// <summary>
        /// Property holding fizz buzz list
        /// </summary>
        public PagedList.IPagedList<string> FizzBuzzList { get; set; }
    }
}