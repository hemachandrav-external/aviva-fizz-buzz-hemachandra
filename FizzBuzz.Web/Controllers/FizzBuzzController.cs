﻿// <copyright file="FizzBuzzController.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System;
using System.Web.Mvc;
using FizzBuzz.Service;
using FizzBuzz.Web.Models;
using PagedList;

namespace FizzBuzz.Web.Controllers
{
    /// <summary>
    /// Controller class to render fizzbuzz data in a view
    /// </summary>
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// Variable holding Grid pagesize
        /// </summary>
        private const int PageSize = 20;

        /// <summary>
        /// Variable holding fizzbuzzservice class
        /// </summary>
        private readonly IFizzBuzzService fizzBuzzService;

        public FizzBuzzController(IFizzBuzzService fizzBuzzService)
        {
            this.fizzBuzzService = fizzBuzzService;
        }

        /// <summary>
        /// Action that renders index page and gets data
        /// </summary>
        [HttpGet]
        public ActionResult Index(int? userInput, int? page)
        {
            if (userInput.HasValue)
            {
                ViewBag.UserInput = Convert.ToInt32(userInput);
                var pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
                var lstWords = fizzBuzzService.GetFizzBuzzData(Convert.ToInt32(userInput));

                return View("Index",
                    new FizzBuzzModel
                    {
                        FizzBuzzList = lstWords.ToPagedList(pageIndex, PageSize),
                        UserInput = ViewBag.UserInput
                    });
            }

            return View("Index");
        }
    }
}