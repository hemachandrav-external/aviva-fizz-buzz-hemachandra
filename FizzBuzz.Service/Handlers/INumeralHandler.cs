﻿// <copyright file="INumeralHandler.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

namespace FizzBuzz.Service.Handlers
{
    public interface INumeralHandler
    {
        /// <summary>
        /// Checks whether this class has rules for specific number
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Returns true if it handles specific number or else false</returns>
        bool CanHandle(int number);

        /// <summary>
        /// Gets new value based on rules
        /// </summary>
        /// <returns>Returns value based on rules</returns>
        string GetResult();
    }
}