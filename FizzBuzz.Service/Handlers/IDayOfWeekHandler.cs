﻿// <copyright file="IDayOfWeekHandler.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System;

namespace FizzBuzz.Service.Handlers
{
    /// <summary>
    /// Interface that handles rules for all days in a week
    /// </summary>
    public interface IDayOfWeekHandler
    {
        /// <summary>
        /// Checks whether this class has rules for specific day of week
        /// </summary>
        /// <param name="dayOfWeek">Indicates specific day of week</param>
        /// <returns>Returns true if it handles specific day of week or else false</returns>
        bool CanHandle(DayOfWeek dayOfWeek);

    }
}
