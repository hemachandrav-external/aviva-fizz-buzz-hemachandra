﻿// <copyright file="DivisibleByThreeNumeralHandlerTests.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using FizzBuzz.Service.Handlers;
using Moq;
using NUnit.Framework;
using System;

namespace FizzBuzz.Service.UnitTests.Handlers
{
    public class DivisibleByThreeNumeralHandlerTests
    {
        /// <summary>
        /// Variable holding mock for IDayOfWeekHandler
        /// </summary>
        private Mock<IDayOfWeekHandler> dayOfWeekHandlerMock;

        /// <summary>
        /// Moq setup method which runs before each test
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            dayOfWeekHandlerMock = new Mock<IDayOfWeekHandler>();
        }

        /// <summary>
        /// Test: Should handle all numbers divisible by 3
        /// </summary>
        [TestCase(9, true)]
        [TestCase(27, true)]
        [TestCase(30, true)]
        [TestCase(19, false)]
        [TestCase(76, false)]
        [TestCase(64, false)]
        public void WhenTheNumber_IsDivisibleByThree_CanHandle_Should_ReturnTrueElseFalse(int number, bool expectedValue)
        {
            var divisibleByThree = new DivisibleByThreeNumeralHandler(dayOfWeekHandlerMock.Object);
            Assert.AreEqual(expectedValue, divisibleByThree.CanHandle(number));
        }

        /// <summary>
        /// Test: When today is  wednesday, GetResult() method should return wizz
        /// </summary>
        [Test]
        public void WhenTodayIsWednesday_GetResult_Should_ReturnWizz()
        {
            var divisibleByThree = new DivisibleByThreeNumeralHandler(dayOfWeekHandlerMock.Object);
            dayOfWeekHandlerMock.Setup(x => x.CanHandle(It.IsAny<DayOfWeek>())).Returns(true);

            Assert.AreEqual("wizz", divisibleByThree.GetResult());
        }

        /// <summary>
        /// Test: When today is not wednesday, GetResult() method should return Fizz
        /// </summary>
        [Test]
        public void WhenTodayIsNotWednesday_GetResult_Should_ReturnFizz()
        {
            var divisibleByThree = new DivisibleByThreeNumeralHandler(dayOfWeekHandlerMock.Object);
            dayOfWeekHandlerMock.Setup(x => x.CanHandle(It.IsAny<DayOfWeek>())).Returns(false);

            Assert.AreEqual("fizz", divisibleByThree.GetResult());
        }
    }
}
