﻿// <copyright file="DivisibleByFiveNumeralHandlerTests.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using FizzBuzz.Service.Handlers;
using Moq;
using NUnit.Framework;
using System;

namespace FizzBuzz.Service.UnitTests.Handlers
{
    public class DivisibleByFiveNumeralHandlerTests
    {     
        /// <summary>
        /// Variable holding mock for IDayOfWeekHandler
        /// </summary>
        private Mock<IDayOfWeekHandler> dayOfWeekHandlerMock;

        /// <summary>
        /// Moq setup method which runs before each test
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            dayOfWeekHandlerMock = new Mock<IDayOfWeekHandler>();
        }
        /// <summary>
        /// Test: Should handle all numbers divisible by 5
        /// </summary>
        [TestCase(10, true)]
        [TestCase(15, true)]
        [TestCase(50, true)]
        [TestCase(19, false)]
        [TestCase(13, false)]
        [TestCase(58, false)]
        public void WhenTheNumber_IsDivisibleByFive_CanHandle_Should_ReturnTrue(int number, bool expectedValue)
        {
            var divisibleByFive = new DivisibleByFiveNumeralHandler(dayOfWeekHandlerMock.Object);
            Assert.AreEqual(expectedValue, divisibleByFive.CanHandle(number));
        }

        /// <summary>
        /// Test: When today is wednesday, GetResult() method should return wuzz
        /// </summary>
        [Test]
        public void WhenTodayIsWednesday_GetResult_Should_ReturnWuzz()
        {
            var divisibleByFive = new DivisibleByFiveNumeralHandler(dayOfWeekHandlerMock.Object);
            dayOfWeekHandlerMock.Setup(x => x.CanHandle(It.IsAny<DayOfWeek>())).Returns(true);

            Assert.AreEqual("wuzz", divisibleByFive.GetResult());
        }

        /// <summary>
        /// Test: When today is not wednesday, GetResult() method should return buzz
        /// </summary>
        [Test]
        public void WhenTodayIsNotWednesday_GetResult_Should_ReturnBuzz()
        {
            var divisibleByFive = new DivisibleByFiveNumeralHandler(dayOfWeekHandlerMock.Object);
            dayOfWeekHandlerMock.Setup(x => x.CanHandle(It.IsAny<DayOfWeek>())).Returns(false);

            Assert.AreEqual("buzz", divisibleByFive.GetResult());
        }
    }
}
