﻿// <copyright file="DayOfWeekHandlerTests.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System;
using FizzBuzz.Service.Handlers;
using NUnit.Framework;

namespace FizzBuzz.Service.UnitTests.Handlers
{
    /// <summary>
    /// Class to test DayOfWeekHandler
    /// </summary>
    [TestFixture()]
    public class DayOfWeekHandlerTests
    {
        private DayOfWeekHandler dayOfWeekHandler;

        /// <summary>
        /// Moq setup method which runs before each test
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            dayOfWeekHandler = new DayOfWeekHandler();
        }

        /// <summary>
        /// Test: CanHandle returns true if today is wednesday
        /// </summary>
        [TestCase(DayOfWeek.Wednesday, true)]
        public void WhenTodayIsWednesday_CanHandle_Should_ReturnTrue(DayOfWeek dayOfWeek, bool expectedValue)
        {
            Assert.AreEqual(expectedValue, dayOfWeekHandler.CanHandle(dayOfWeek));
        }

        /// <summary>
        /// Test: CanHandle returns false if today is not wednesday
        /// </summary>
        [TestCase(DayOfWeek.Monday, false)]
        [TestCase(DayOfWeek.Tuesday, false)]
        [TestCase(DayOfWeek.Thursday, false)]
        [TestCase(DayOfWeek.Friday, false)]
        [TestCase(DayOfWeek.Saturday, false)]
        [TestCase(DayOfWeek.Sunday, false)]
        public void WhenTodayisNotWednesday_CanHandle_Should_ReturnFalse(DayOfWeek dayOfWeek, bool expectedValue)
        {
            Assert.AreEqual(expectedValue, dayOfWeekHandler.CanHandle(dayOfWeek));
        }
    }
}
